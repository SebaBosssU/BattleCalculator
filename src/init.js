require([
    'two/ready',
    'two/simulator',
    'two/simulator/ui'
], function (
    ready,
    BattleCalculator
) {
    if (BattleCalculator.initialized) {
        return false
    }

    ready(function () {
        BattleCalculator.init()
        BattleCalculator.interface()
        BattleCalculator.run()
    })
})
