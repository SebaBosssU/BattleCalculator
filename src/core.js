define('two/simulator', [
    'two/locale',
    'two/ready'
], function (
    Locale,
    ready
) {
    var BattleCalculator = {}
    BattleCalculator.version = '__simulator_version'
    BattleCalculator.init = function () {
        Locale.create('simulator', __simulator_locale, 'pl')

        BattleCalculator.initialized = true
    }
    BattleCalculator.run = function () {
        if (!BattleCalculator.interfaceInitialized) {
            throw new Error('BattleCalculator interface not initialized')
        }
        ready(function () {
            $player = modelDataService.getSelectedCharacter()
        }, ['initial_village'])
    }
    return BattleCalculator
})